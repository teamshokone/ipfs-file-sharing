How to add a new peer to exchange files with:

1. Start daemon
2. Open new terminal and type “ipfs config show”
3. Find your peer multihash and write it down or copy it somewhere
4. Go to second computer and do the same thing

5. On computer 1, type the following in the terminal: “ipfs bootstrap add /ip4/IPaddress/tcp/4001/ipfs/multihash” (replace “IPaddress” with computer 2’s ipv4 address and replace “multihash” with computer 2’s peer multihash)
6. Hit enter and the new peer will be added to your computer.
7. Repeat steps 5 & 6 on computer 2 with computer 1’s ipv4 address and multihash.

Adding another computer as a bootstrap peer is the only way I’ve been able to exchange files between systems.
This process will need to be implemented in a storage app in order to access files from other computers (sharing the multihash is not enough to retrieve the file).
Adding a new peer is similar to adding a "friend" or recognized account on Dropbox and Drive.